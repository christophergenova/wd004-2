function getSum(num1, num2){
	const sum = num1 + num2;
	return sum;

}

function getDiff(num1, num2){
	const diff = num1 - num2;
	return diff;
	
}

function getMulti(num1, num2){
	const multi = num1 * num2;
	return multi;
	
}

function getDiv(num1, num2){
	const div = num1 / num2;
	return div;
	
}

// stretch goal

function getCylinderVolume(r, h){
	//radius = pi
	const radius = 3.1416;

	//multiply radius by itself
	const r1 = r * r;

	const volume = radius * r1 * h;
	return volume;

	//some approach

	
}

// formula: b1+b2/2 * h
function getTrapezoidArea(base1, base2, height){

	const sumOfBases = base1 + base2;
	const halfOfBases = sumOfbases / 2;
	return area;

}

function getTotalAmount(cost, quantity, discount, tax) {

	// converted the discount into decimal

	const convertedDiscount = discount / 100;
	// converted tax into decimal
	const convertedTax = tax / 100;
	//got the total amount without discount
	const amountWithoutDiscount = cost * quantity;
	//computed discount
	const discountRate = amountWithoutDiscount * convertedDiscount;

	//deduct discount
	const discountedAmount = amountWithoutDiscount - discountRate;
	//compute tax
	const taxRate = discountedAmount * convertedTax;

	//finally, add the tax to get the total amount;
	const totalAmount = discountedAmount + taxRate;

	return totalAmount;

}