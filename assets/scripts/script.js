//
// Variabls:
// Let and Const
//
// Data types:
// String - "",
// Number - we can use arithmetic ooperators
// Boolean - true/false
// 
// Arithmetic Operators:
// +, -, *, /, %
// 
// Modulo - remainder
//
// Assignment Operator: =
//
// let number = 4;
// number *= 4; // the value is 8
// number -= 2; // the value is 6


// const num1 = 2;
// const num2 = 4;

// const sum1 = num1 + num2;
// console.log(sum1);

// const num3 = 5;
// const num4 = 3;

// const sum2 = num3 + num4;
// console.log(sum2);

// const num5 = 11;
// const num6 = 3;

// const sum3 = num5 + num6;
// console.log(sum3);

// const num 
//
// Dry principle
// Don't Repeat Yourself
//
// That is the use of Functions
//
// Functions
// subprogram that is designed to do a particular function
//
// We need a function that will add two numbers

// Syntax:
//
// basic syntax of our function
// function nameOfTheFunction(){
// 	// task or what your function should do

// }

// rember, just like your variables, function names should be descriptive.
//
// function names/ variables should be in camel case.
//
// accepts parameters within this ()
//
// parameters - these are the values that the function needs in order to do its task
//
// camel case is first word is lowercase the second is Upercase
//
// Function Decleration
// if the function does not have a return value, it will return an undefined data
function getSum(num1, num2){
	const sum = num1 + num2;
	console.log(sum);
	return sum;

}

// function call
// Arguments - these are the values that we will send to the function
// getSum(2,3); //2 is num1, 3 is num2
// getSum(4,5);
// getSum(9,8);


const ageSum = getSum(15,18)
console.log(ageSum);





